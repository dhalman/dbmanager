package DBManager

import (
	"os"

	"github.com/itsmontoya/turtle"
)

var (
	// Error messaging
	errorDbInit = "Unable to create database"

	floatSize             = 32
	defaultCollectionSize = 10
)

// DBManager handles storage for other managers
type DBManager struct {
	db   *turtle.Turtle
	mfn  turtle.MarshalFn
	ufn  turtle.UnmarshalFn
	path string
}

// New initializes and returns a DBManager object for a given type
func New(filename string, mfn turtle.MarshalFn, ufn turtle.UnmarshalFn) (*DBManager, error) {
	newDb, err := turtle.New(filename, "./db/", mfn, ufn)
	if err != nil {
		return nil, err
	}

	return &DBManager{newDb, mfn, ufn, "./db/" + filename}, err
}

// Delete will remove an entry
func (dbm *DBManager) Delete(key string) error {
	return dbm.db.Update(func(tx turtle.Txn) error {
		return tx.Delete(key)
	})
}

// Set will create/overwrite
func (dbm *DBManager) Set(key string, value turtle.Value) error {
	return dbm.db.Update(func(tx turtle.Txn) error {
		return tx.Put(key, value)
	})
}

// Get will return an entry
func (dbm *DBManager) Get(key string) (turtle.Value, error) {
	var value turtle.Value
	var err error

	err = dbm.db.Read(func(tx turtle.Txn) error {
		value, err = tx.Get(key)

		return err
	})

	return value, err
}

func filter(val turtle.Value) bool {
	return true
}

// List will return all entries
func (dbm *DBManager) List() (map[string]interface{}, error) {
	items := make(map[string]interface{}, defaultCollectionSize)

	error := dbm.db.Read(func(tx turtle.Txn) error {
		// Assume bucket exists and has keys
		tx.ForEach(func(key string, value turtle.Value) (end bool) {
			if value != nil {
				items[key] = value
			}

			return false
		})

		return nil
	})

	return items, error
}

// Clear deletes db file and reinitializes
func (dbm *DBManager) Clear() error {
	err := dbm.db.Close()
	if err != nil {
		print(err)
	}

	err = os.Remove(dbm.path + ".tdb")
	if err != nil {
		print(err)
	}

	newDb, err := turtle.New(dbm.path, ".", dbm.mfn, dbm.ufn)

	if err != nil {
		print(err)
	}

	dbm.db = newDb

	return nil
}
